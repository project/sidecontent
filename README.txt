********************************************************************
D R U P A L    M O D U L E
********************************************************************
Name: Side Content module 
Drupal: 6.x
Author: Robert Castelo
Contact: www.codepositive.com

********************************************************************
DESCRIPTION:


      * When creating or editing a page, this module allows content to be added to a side bar block.

      * The content will only be seen when viewing the page (node) that it's been added to. 

      * This works on any kind of page - book, blog, forum...

      * The block on each page can have it's own title.

      * The text is searchable, and can be in a variety of formats (depending on filter permissions).

      * The block created can have a title, or no title - whichever you prefer.

      * Various options available for how it's fields are included on edit page forms.
      
      * Print Friendly Pages module integration, option to not show sidecontent on print pages




********************************************************************
INSTALLATION:


      Note: It is assumed that you have Drupal up and running.  Be sure to
      check the Drupal web site if you need assistance.  If you run into
      problems, you should always read the INSTALL.txt that comes with the
      Drupal package and read the online documentation.

	1. Place the entire sidecontent directory into your Drupal modules/directory.
	    Or even better: sites/default/modules

	2. Enable the sidecontent module by navigating to:

	   Administer > Site building > Modules

	Click the 'Save configuration' button at the bottom to commit your changes.



********************************************************************
CONFIGURATION

       1. Configure the module on the web page:
           Administer > Site building > Blocks

           Click 'configure' on the sidecontent block's row.

           Select your prefered display options.
           
           Save configuration.
           
           Once it's configured enable the block.
    
        2. Set access permissions for side content blocks:
            Administer > User Management > Access control

        3. Set which content types can have a sidecontent block:
            Administer > Content Management > Content Types

            Click on a content type and enable/disable Sidecontent.
            
        4. Create or edit a page, page must be of an enabled content type (see 3. above).
        
        
********************************************************************
THANKS:

Gabriel Birke - various form editing improvements
http://www.d-scribe.de/

Pierre Mazoyer (PierreM) - Drupal 5 update
http://www.viasun.fr

geekonek - postgres support Drupal 5





